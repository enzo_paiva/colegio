<?php

namespace App\Http\Controllers;

use App\Horario;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    

    public function index()
    {
        $horas = Horario::all();
        return view('admin.horario-aula', compact('horas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create-horario-aula');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hora = new Horario();
        $hora->inicio = $request->input('inicio');
        $hora->fim = $request->input('fim');
        $hora->save();
        return redirect()->route('hora_Aula');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hora = Horario::find($id);
        if (isset($hora)) {
            return view('admin.edit-horario-aula', compact('hora'));
        }
        return redirect()->route('hora_Aula');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hora = Horario::find($id);
        if(isset($hora)){
            $hora->dia = $request->input('dia');
            $hora->inicio = $request->input('inicio');
            $hora->fim = $request->input('fim');
            $hora->save();
        }
        return redirect()->route('hora_Aula');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horas = Horario::find($id);
        if(isset($horas)){
            $horas->delete();
        }
        return redirect()->route('hora_Aula');
    }
}
