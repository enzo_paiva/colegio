<?php

namespace App\Http\Controllers;

use App\TA;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlunoSalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $ats = DB::table('t_a_s')
        ->join('turmas','t_a_s.turma_id','=','turmas.id')
        ->join('users','t_a_s.aluno_id','=','users.id')
        ->select(
            't_a_s.id as tas_id',
            'users.name as aluno_nome',
            'users.cpf as aluno_cpf',
            'turmas.codigo_turma as turma_nome',
            't_a_s.created_at as tas_criado',
            't_a_s.updated_at as tas_atualizado',
        )
        ->get();
        return view('admin.aluno_sala', compact('ats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create-aluno-sala');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aluno = new TA();
        $aluno->turma_id = $request->input('turma');
        $aluno->aluno_id = $request->input('aluno');
        $aluno->save();
        return redirect()->route('aluno_sala');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $as=DB::table('t_a_s')->get();
        return view('admin.edit-aluno-sala', compact('as'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $at = TA::find($id);
        if (isset($at)) {
            return view('admin.edit-aluno-sala',compact('at'));
        }
        return redirect()->route('aluno_sala');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $at = TA::find($id);
        if (isset($at)) {
            $at->turma_id = $request->input('turma');
            $at->aluno_id = $request->input('aluno');
            $at->save();
        }
        return redirect()->route('aluno_sala');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $at= TA::find($id);
        if (isset($at)) {
            $at->delete();
        }
        return redirect()->route('aluno_sala');
    }
}
