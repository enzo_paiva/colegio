<?php

namespace App\Http\Controllers;

use App\Professor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $profs = DB::table('professors')
        ->join('materias','professors.id_materia','=','materias.id')
        ->select(
            'professors.id as prof_id',
            'professors.nome as prof_nome',
            'professors.cpf as prof_cpf',
            'materias.nome as mat_nome',
            'professors.created_at as prof_criado',
            'professors.updated_at as prof_atualizado',
        )
        ->get();
        return view('admin.professor',compact('profs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mat = Professor::find($id);
        if (isset($mat)) {
            return view('admin.edit-professor',compact('mat'));
        }
        return redirect()->route('admin.prof-list');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mat = Professor::find($id);
        if (isset($mat)) {
            $mat->nome = $request->input('name');
            $mat->cpf = $request->input('cpf');
            $mat->id_materia = $request->input('disciplina');
            $mat->email = $request->input('email') ;
            $mat->password = Hash::make($request->input('password'));
            $mat->save();
        }
        return redirect()->route('admin.prof-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mat = Professor::find($id);
        if (isset($mat)) {
            $mat->delete();
        }
        return redirect()->route('admin.prof-list');
    }
}
