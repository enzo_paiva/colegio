<?php

namespace App\Http\Controllers;

use App\DiaAula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiaAulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $turmas = DB::table('t_a_s')
        ->join('turmas','t_a_s.turma_id','=','turmas.id')
        ->select('turmas.codigo_turma as turma_nome')
        ->get();
        $hd = DB::table('dia_aulas')
        ->join('horarios','dia_aulas.hora_id','=','horarios.id')
        ->join('dias','dia_aulas.dia_id','=','dias.id')
        ->join('materias','dia_aulas.id_materia','=','materias.id')
        ->join('professors','dia_aulas.id_professor','=','professors.id')
        ->select(
            'dia_aulas.id as da_id',
            'dias.nome as d_nome',
            'professors.nome as n_prof',
            'materias.nome as m_disciplina',
            'horarios.inicio as h_inicio',
            'horarios.fim as h_fim',
            'dia_aulas.created_at as da_criado',
            'dia_aulas.updated_at as da_atualizado',
        )
        ->get();
        return view('admin.dia-aula',compact('hd', 'turmas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prof_mat = DB::table('professors')
        ->join('materias','professors.id_materia','=','materias.id')
        ->select(
            'professors.id as prof_id',
            'professors.nome as prof_nome',
            'materias.id as mat_id',
            'materias.nome as mat_nome',
        )
        ->get();
        
        $hd = DB::table('dia_aulas')
        ->join('horarios','dia_aulas.hora_id','=','horarios.id')
        ->join('dias','dia_aulas.dia_id','=','dias.id')
        ->join('materias','dia_aulas.id_materia','=','materias.id')
        ->join('professors','dia_aulas.id_materia','=','professors.id_materia')
        ->select(
            'dia_aulas.id as da_id',
            'dias.nome as d_nome',
            'professors.nome as n_prof',
            'materias.nome as m_disciplina',
            'horarios.inicio as h_inicio',
            'horarios.fim as h_fim',
            'dia_aulas.created_at as da_criado',
            'dia_aulas.updated_at as da_atualizado',
        )
        ->get();
        return view('admin.create-aula-dia',compact('hd','prof_mat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $diaAula = new DiaAula();
        // $diaAula->id = $request->input('id');
        $diaAula->dia_id = $request->input('dia');
        $diaAula->hora_id = $request->input('horario');
        $diaAula->id_materia = $request->input('materia');
        $diaAula->id_professor = $request->input('professor');
        $diaAula->id_at = $request->input('turma');
        $diaAula->save();
        return redirect()->route('dia_Aula');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prof_mat = DB::table('professors')
        ->join('materias','professors.id_materia','=','materias.id')
        ->select(
            'professors.id as prof_id',
            'professors.nome as prof_nome',
            'materias.id as mat_id',
            'materias.nome as mat_nome',
        )
        ->get();
        
        // $hd = DB::table('dia_aulas')
        // ->join('horarios','dia_aulas.hora_id','=','horarios.id')
        // ->join('dias','dia_aulas.dia_id','=','dias.id')
        // ->join('materias','dia_aulas.id_materia','=','materias.id')
        // ->join('professors','dia_aulas.id_materia','=','professors.id_materia')
        // ->select(
        //     'dia_aulas.id as da_id',
        //     'dias.nome as d_nome',
        //     'professors.nome as n_prof',
        //     'materias.nome as m_disciplina',
        //     'horarios.inicio as h_inicio',
        //     'horarios.fim as h_fim',
        //     'dia_aulas.created_at as da_criado',
        //     'dia_aulas.updated_at as da_atualizado',
        // )
        // ->get();
        // return view('admin.edit-aula-dia',compact('hd','prof_mat'));
        // // $prof_mat = DB::table('professors')
        // // ->join('materias','professors.id_materia','=','materias.id')
        // // ->select(
        // //     'professors.id as prof_id',
        // //     'professors.nome as prof_nome',
        // //     'materias.id as mat_id',
        // //     'materias.nome as mat_nome',
        // // )
        // // ->get();

        // $hr = DiaAula::all();
        
        // // $ad = DB::table('dia_aulas')
        // // ->join('horarios','dia_aulas.hora_id','=','horarios.id')
        // // ->join('dias','dia_aulas.dia_id','=','dias.id')
        // // ->join('materias','dia_aulas.id_materia','=','materias.id')
        // // ->join('professors','dia_aulas.id_professor','=','professors.id')
        // // ->select(
        // //     'dia_aulas.id as da_id',
        // //     'dias.nome as d_nome',
        // //     'horarios.id as hora_id',
        // //     'professors.nome as n_prof',
        // //     'materias.nome as m_disciplina',
        // //     'horarios.inicio as h_inicio',
        // //     'horarios.fim as h_fim',
        // //     'dia_aulas.created_at as da_criado',
        // //     'dia_aulas.updated_at as da_atualizado',
        // // )
        // // ->get();

        $hdd = DB::table('dia_aulas')
        ->join('horarios','dia_aulas.hora_id','=','horarios.id')
        ->join('dias','dia_aulas.dia_id','=','dias.id')
        ->join('materias','dia_aulas.id_materia','=','materias.id')
        ->join('professors','dia_aulas.id_professor','=','professors.id')
        ->select(
            'dia_aulas.id as da_id',
            'dias.nome as d_nome',
            'dia_aulas.hora_id as hora_id',
            'dia_aulas.hora_id as dia_id',
            'dia_aulas.id_materia as materia_id',
            'dia_aulas.id_professor as professor_id',
            'professors.nome as n_prof',
            'materias.nome as m_disciplina',
            'horarios.inicio as h_inicio',
            'horarios.fim as h_fim',
            'dia_aulas.created_at as da_criado',
            'dia_aulas.updated_at as da_atualizado',
        )
        ->get();

        return view('admin.edit-aula-dia',compact('hdd', 'prof_mat'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hd = DiaAula::find($id);
        if (isset($hd)) {
            $hd->delete();
        }
        return redirect()->route('dia_Aula');
    }
}
