<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dias')->insert(['nome'=>'domingo']);
        DB::table('dias')->insert(['nome'=>'segunda']);
        DB::table('dias')->insert(['nome'=>'terça']);
        DB::table('dias')->insert(['nome'=>'quarta']);
        DB::table('dias')->insert(['nome'=>'quinta']);
        DB::table('dias')->insert(['nome'=>'sexta']);
        DB::table('dias')->insert(['nome'=>'sabado']);
    }
}
