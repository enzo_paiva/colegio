<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiaAulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dia_aulas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('dia_id');
            $table->foreign('dia_id')->references('id')->on('dias');
            $table->unsignedBigInteger('hora_id');
            $table->foreign('hora_id')->references('id')->on('horarios');
            $table->unsignedBigInteger('id_materia');
            $table->foreign('id_materia')->references('id')->on('materias'); 
            $table->unsignedBigInteger('id_professor');
            $table->foreign('id_professor')->references('id')->on('professors');           
            $table->unsignedBigInteger('id_at');
            $table->foreign('id_at')->references('id')->on('t_a_s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dia_aulas');
    }
}
