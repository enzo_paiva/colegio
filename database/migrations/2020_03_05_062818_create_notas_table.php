<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bimestre1');
            $table->integer('bimestre2');
            $table->integer('bimestre3');
            $table->integer('bimestre4');
            $table->integer('nota_final');
            $table->integer('situacao');

            $table->unsignedBigInteger('id_at')->unsigned();
            $table->foreign('id_at')->references('id')->on('t_a_s');

            $table->unsignedBigInteger('id_professor_turma');
            $table->foreign('id_professor_turma')->references('id')->on('dia_aulas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
