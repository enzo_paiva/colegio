/*!
    * Start Bootstrap - SB Admin v6.0.0 (https://startbootstrap.com/templates/sb-admin)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-sb-admin/blob/master/LICENSE)
    */
   (function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
            if (this.href === path) {
                $(this).addClass("active");
            }
        });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);

$(document).ready(function(){
    $("#time").inputmask("h:s",{ "placeholder": "hh/mm" });
});

$(document).ready(function(){
    $("#time2").inputmask("h:s",{ "placeholder": "hh/mm" });
});


$(document).ready(function(){
    $("#cpf").mask("999.999.999-99");
  });

  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.s1').select2();
});

$(document).ready(function() {
    $('.s2').select2();
});

$(document).ready(function() {
    $('.s3').select2();
});

