@extends('layouts.navbaradmin')

@section('content')
    <!------Body------>
    <div class="container-fluid">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>
        <div class="row">
            
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">Hora e Dia de Aula</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="{{ route('dia_Aula.create') }}">Registrar Novo</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-table mr-1"></i>Lista de Dias e Aulas</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Dia</th>
                            <th>Inicio</th>
                            <th>Fim</th>
                            <th>Professor</th>
                            <th>Disciplina</th>
                            <th>Turma</th>
                            <th>criado em</th>
                            <th>atualizado em</th>
                            {{-- <th>editar</th> --}}
                            <th>apagar</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @foreach ($hd ?? '' as $h)
                            <tr>
                                <td>{{$h->da_id}}</td>
                                <td>{{$h->d_nome}}</td>
                                <td>{{$h->h_inicio}}</td>
                                <td>{{$h->h_fim}}</td>
                                <td>{{$h->n_prof}}</td>
                                <td>{{$h->m_disciplina}}</td>
                                <td>{{$h->turma_nome}}</td>
                                <td>{{$h->da_criado}}</td>
                                <td>{{$h->da_atualizado}}</td>
                                {{-- <td><a class="btn btn-warning" href="/DiaAula/edit/{{$h->da_id}}" role="button">
                                    <i class="fas fa-edit"></i>
                                  </a></td> --}}
                                  <td><a class="btn btn-danger glyphicon glyphicon-pencil" href="/DiaAula/delete/{{$h->da_id}}" role="button">
                                    <i class="far fa-trash-alt"></i></a></td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection