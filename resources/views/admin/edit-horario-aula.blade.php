@extends('layouts.navbaradmin')

@section('content')
<!------Body------>
<div class="container-fluid">
    <h1 class="mt-4">Formulário de horario aula</h1>
    <p>Preencha o formulario abaixo para o preenchimento da horas que ocorrerão as aulas durente o dia!</p>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    

    <div class="container">
        
        <a class="float-right" href="{{ route('hora_Aula') }}"> <i class="fas fa-arrow-left float-left"></i></a>
    <form action="/HoraAula/update/{{$hora->id}}"  method="POST" > 
            @csrf

            <div class="form-group">
                
                <div class="col-md-4 ">
                    <label>Inicio</label>
                    <input type="text" class="form-control" name="inicio" value="{{$hora->inicio}}" id="time" value=""/> 
                </div>
              </div>

              <div class="form-group">
                
                <div class="col-md-4 ">
                    <label >Fim</label>
                    <input type="text" class="form-control" name="fim" value="{{$hora->fim}}" id="time2" value=""/>
                </div>
              </div>    
    
            <div class="form-group">
                <div class="col-md-4">
                    <input type="submit" value="Enviar" class="btn btn-primary">
                    
                </div>
            </div>
        </form> 
    </div>
</div>
@endsection

