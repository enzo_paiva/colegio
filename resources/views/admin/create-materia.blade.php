@extends('layouts.navbaradmin')

@section('content')
<!------Body------>
<div class="container-fluid">
    <h1 class="mt-4">Formulário de disciplinas</h1>
    <p>Preencha o formulario abaixo para o preenchimento das disciplinas que a escola ofertará!</p>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    

    <div class="container">
        
        <a class="float-right" href="{{ route('materia') }}"> <i class="fas fa-arrow-left float-left"></i></a>
        <form action="/Materia/Store"  method="POST" > 
            @csrf

            <div class="form-group row">
                
                <div class="col-md-6 offset-md-3 ">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="name" value=""/> 
                </div>
              </div>   
    
            <div class="form-group">
                <div class="col-md-6 offset-md-3">
                    <input type="submit" value="Enviar" class="btn btn-primary">
                    
                </div>
            </div>
        </form> 
    </div>
</div>
@endsection

