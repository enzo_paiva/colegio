@extends('layouts.navbaradmin')

@section('content')
<!------Body------>
<div class="container-fluid">
    <h1 class="mt-4">Formulário de sala e aluno</h1>
    <p>Preencha o formulario abaixo para o preenchimento do aluno em uma sala de aula!</p>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    

    <div class="container">
        
        <a class="float-right" href="{{ route('aluno_sala') }}"> <i class="fas fa-arrow-left float-left"></i></a>
        <form action="/Aluno_Sala/Store"  method="POST" > 
            @csrf

            

            <div class="form-group row">
                
                <div class="col-md-4 offset-md-3 ">
                    <label>Aluno</label>
                    <select class="form-control s1" name="aluno" id="exampleFormControlSelect1">
                        @foreach(App\User::all() as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}} - CPF: {{$cat->cpf}}</option>
                        @endforeach
                      </select>
                </div>
              </div>   
    
            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <label>Turma</label>
                    <select class="form-control s2" name="turma" id="s1">
                        @foreach(App\Turma::all() as $cat)
                            <option value="{{$cat->id}}">{{$cat->codigo_turma}}</option>
                        @endforeach
                      </select>
                    
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <input type="submit" value="Enviar" class="btn btn-primary">
                    
                </div>
            </div>
        </form> 
    </div>
</div>
@endsection

