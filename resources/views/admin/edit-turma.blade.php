@extends('layouts.navbaradmin')

@section('content')
<!------Body------>
<div class="container-fluid">
    <h1 class="mt-4">Formulário de turmas</h1>
    <p>Preencha o formulario abaixo para o preenchimento das turmas existentes na escola!</p>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    

    <div class="container">
        
        <a class="float-right" href="{{ route('turma') }}"> <i class="fas fa-arrow-left float-left"></i></a>
        <form action="/Turma/update/{{$turma->id}}"  method="POST" > 
            @csrf

            <div class="form-group row">
                
                <div class="col-md-6 offset-md-3 ">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="turma" value="{{$turma->codigo_turma}}"/> 
                </div>
              </div>   
    
            <div class="form-group">
                <div class="col-md-6 offset-md-3">
                    <input type="submit" value="Enviar" class="btn btn-primary">
                    
                </div>
            </div>
        </form> 
    </div>
</div>
@endsection

