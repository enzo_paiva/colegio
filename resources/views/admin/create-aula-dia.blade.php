@extends('layouts.navbaradmin')

@section('content')
<!------Body------>
<div class="container-fluid">
    <h1 class="mt-4">Formulário de hora e dia</h1>
    <p>Preencha o formulario abaixo para o preenchimento do dia e hora da aula!</p>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    

    <div class="container">
        
        <a class="float-right" href="{{ route('dia_Aula') }}"> <i class="fas fa-arrow-left float-left"></i></a>
        <form action="/DiaAula/Store"  method="POST" > 
            @csrf

            {{-- <div class="form-group row">
                
                <div class="col-md-4 offset-md-3 ">
                    <label>Identificador (informe sempre um numero unico)</label>
                    <input type="integer" class="form-control" name="id" value=""/> 
                </div>
              </div> --}}

            <div class="form-group row">
                
                <div class="col-md-4 offset-md-3 ">
                    <label>Horario</label>
                    <select class="form-control s1" name="horario" id="exampleFormControlSelect1">
                        @foreach(App\Horario::all() as $cat)
                            <option value="{{$cat->id}}">Inicio: {{$cat->inicio}} - Fim: {{$cat->fim}}</option>
                        @endforeach
                      </select>
                </div>
              </div>   
    
            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <label>Dia</label>
                    <select class="form-control s2" name="dia" id="s2">
                        @foreach(App\Dia::all() as $cat)
                            <option value="{{$cat->id}}">{{$cat->nome}}</option>
                        @endforeach
                      </select>
                    
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <label>Materia</label>
                    <select class="form-control s3" name="materia" id="s3">
                        @foreach(App\Materia::all() as $cat)
                            <option value="{{$cat->id}}">{{$cat->nome}}</option>
                        @endforeach
                      </select>
                    
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <label>Professor</label>
                    <select class="form-control s2" name="professor" id="s1">
                        @foreach($prof_mat as $cat)
                            <option value="{{$cat->prof_id}}">Nome: {{$cat->prof_nome}} - Disciplina: {{$cat->mat_nome}}</option>
                        @endforeach
                      </select>
                    
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <label>Turma</label>
                    <select class="form-control s2" name="turma" id="s1">
                        @foreach(App\Turma::all() as $cat)
                            <option value="{{$cat->id}}"> {{$cat->codigo_turma}}</option>
                        @endforeach
                      </select>
                    
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4 offset-md-3">
                    <input type="submit" value="Enviar" class="btn btn-primary">
                    
                </div>
            </div>
        </form> 
    </div>
</div>
@endsection

