@extends('layouts.navbaradmin')

@section('content')
<!------Body------>
<div class="container-fluid">
    <h1 class="mt-4">Formulário de hora e dia</h1>
    <p>Preencha o formulario abaixo para o preenchimento do dia e hora da aula!</p>
    
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    

    <div class="container">
        
        <a class="float-right" href="{{ route('dia_Aula') }}"> <i class="fas fa-arrow-left float-left"></i></a>
    @foreach ($hdd as $hd)
    
    <form action="/DiaAula/update/{{$hd->da_id}}"  method="POST" > 
        @csrf

        <div class="form-group row">
            
            <div class="col-md-4 offset-md-3 ">
                <label>Identificador (informe sempre um numero unico)</label>
            <input type="integer" class="form-control" name="id" disabled value="{{$hd->da_id}}"/> 
            </div>
          </div>
          
          <div class="form-group row">
            
            <div class="col-md-4 offset-md-3 ">
                <label>Horario</label>
                <select class="form-control s1" name="horario" id="exampleFormControlSelect1">
                    @foreach(App\Horario::all() as $cat)
                    <option value="{{$cat->id}}"
                        @if ($cat->id==$hd->hora_id)
                             selected="selected"
                             @endif>{{$cat->inicio}} - {{$cat->fim}}</option>
                          @endforeach
                  </select>
            </div>
          </div>  

          <div class="form-group row">
            
            <div class="col-md-4 offset-md-3 ">
                <label>Dia</label>
                <select class="form-control s1" name="dia" id="exampleFormControlSelect1">
                    @foreach(App\Dia::all() as $cat)
                    <option value="{{$cat->id}}"
                        @if ($cat->id==$hd->dia_id)
                             selected="selected"
                             @endif>{{$cat->nome}}</option>
                          @endforeach
                  </select>
            </div>
          </div>

          <div class="form-group row">
            
            <div class="col-md-4 offset-md-3 ">
                <label>Materia</label>
                <select class="form-control s1" name="materia" id="exampleFormControlSelect1">
                    @foreach(App\Materia::all() as $cat)
                    <option value="{{$cat->id}}"
                        @if ($cat->id==$hd->materia_id)
                             selected="selected"
                             @endif>{{$cat->nome}}</option>
                          @endforeach
                  </select>
            </div>
          </div>

          <div class="form-group row">
            
            <div class="col-md-4 offset-md-3 ">
                <label>Professor</label>
                <select class="form-control s1" name="professor" id="exampleFormControlSelect1">
                    @foreach($prof_mat as $cat)
                    <option value="{{$cat->prof_id}}"
                        @if ($cat->prof_id==$hd->professor_id)
                             selected="selected"
                             @endif>{{$cat->prof_nome}}</option>
                          @endforeach
                  </select>
            </div>
          </div>

        <div class="form-group row">
            <div class="col-md-4 offset-md-3">
                <input type="submit" value="Enviar" class="btn btn-primary">
                
            </div>
        </div>
    </form> 
    @endforeach
        
    </div>
</div>
@endsection

