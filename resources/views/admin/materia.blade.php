@extends('layouts.navbaradmin')

@section('content')
    <!------Body------>
    <div class="container-fluid">
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>
        <div class="row">
            
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">Disciplinas</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="{{ route('materia.create') }}">Criar</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-table mr-1"></i>Lista de Disciplinas</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>criado em</th>
                            <th>atualizado em</th>
                            <th>editar</th>
                            <th>apagar</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @foreach ($materias ?? '' as $materia)
                            <tr>
                                <td>{{$materia->id}}</td>
                                <td>{{$materia->nome}}</td>
                                <td>{{$materia->created_at}}</td>
                                <td>{{$materia->updated_at}}</td>
                                <td><a class="btn btn-warning" href="/Materia/edit/{{$materia->id}}" role="button">
                                    <i class="fas fa-edit"></i>
                                  </a></td>
                                  <td><a class="btn btn-danger glyphicon glyphicon-pencil" href="/Materia/delete/{{$materia->id}}" role="button">
                                    <i class="far fa-trash-alt"></i></a></td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

