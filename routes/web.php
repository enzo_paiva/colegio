<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


#admin aluno
Route::get('/Aluno','AlunoController@index')->name('aluno');
Route::get('/Aluno/edit/{id}', 'AlunoController@edit')->name('aluno.edit');
Route::post('/Aluno/update/{id}', 'AlunoController@update');
Route::get('/Aluno/delete/{id}', 'AlunoController@destroy');

#admin aluno-sala
Route::get('/Aluno_Sala','AlunoSalaController@index')->name('aluno_sala');
Route::get('/Aluno_Sala/Create', 'AlunoSalaController@create')->name('aluno_sala.create');
Route::post('/Aluno_Sala/Store', 'AlunoSalaController@store');
Route::get('/Aluno_Sala/edit/{id}', 'AlunoSalaController@edit')->name('aluno_sala.edit');
Route::post('/Aluno_Sala/update/{id}', 'AlunoSalaController@update');
Route::get('/Aluno_Sala/delete/{id}', 'AlunoSalaController@destroy');

#admin login
Route::get('/admin', 'AdminController@index')->name('admin.dahsborard');
Route::get('/admin/login', 'Auth\AdminLoginController@index')->name('admin.Login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.Login.Submit');
Route::get('/admin/register', 'Auth\AdminRegisterController@registeradmin')->name('admin.register');
Route::post('/registeradmin', 'Auth\AdminRegisterController@store');

#admin professor
Route::get('/admin/professor/lista', 'AdminProfessorController@index')->name('admin.prof-list');
Route::get('/professor/register', 'Auth\ProfessorRegisterController@index')->name('professor.create');
Route::get('/admin/professor/edit/{id}', 'AdminProfessorController@edit')->name('admin.prof-edit');
Route::post('/admin/professor/update/{id}', 'AdminProfessorController@update');
Route::get('/admin/professor/delete/{id}', 'AdminProfessorController@destroy');

#professor login
Route::get('/professor', 'ProfessorController@index')->name('professor.dahsborard');
Route::get('/professor/login', 'Auth\ProfessorLoginController@index')->name('professor.Login');
Route::post('/professor/login', 'Auth\ProfessorLoginController@login')->name('professor.Login.Submit');
Route::post('/registerprofessor', 'Auth\ProfessorRegisterController@store');

#Hora Aula
Route::get('/HoraAula', 'HorarioController@index')->name('hora_Aula');
Route::get('/HoraAula/Create', 'HorarioController@create')->name('hora_Aula.create');
Route::post('/HoraAula/Store', 'HorarioController@store');
Route::get('/HoraAula/edit/{id}', 'HorarioController@edit')->name('hora_Aula.edit');
Route::post('/HoraAula/update/{id}', 'HorarioController@update');
Route::get('/HoraAula/delete/{id}', 'HorarioController@destroy');

#Dia Aula
Route::get('/DiaAula', 'DiaAulaController@index')->name('dia_Aula');
Route::get('/DiaAula/Create', 'DiaAulaController@create')->name('dia_Aula.create');
Route::post('/DiaAula/Store', 'DiaAulaController@store');
Route::get('/DiaAula/edit/{id}', 'DiaAulaController@edit')->name('dia_Aula.edit');
Route::post('/DiaAula/update/{id}', 'DiaAulaController@update');
Route::get('/DiaAula/delete/{id}', 'DiaAulaController@destroy');

#Disciplinas
Route::get('/Materia', 'MateriaController@index')->name('materia');
Route::get('/Materia/Create', 'MateriaController@create')->name('materia.create');
Route::post('/Materia/Store', 'MateriaController@store');
Route::get('/Materia/edit/{id}', 'MateriaController@edit')->name('materia.edit');
Route::post('/Materia/update/{id}', 'MateriaController@update');
Route::get('/Materia/delete/{id}', 'MateriaController@destroy');

#turma
Route::get('/Turma', 'TurmaController@index')->name('turma');
Route::get('/Turma/Create', 'TurmaController@create')->name('turma.create');
Route::post('/Turma/Store', 'TurmaController@store');
Route::get('/Turma/edit/{id}', 'TurmaController@edit')->name('turma.edit');
Route::post('/Turma/update/{id}', 'TurmaController@update');
Route::get('/Turma/delete/{id}', 'TurmaController@destroy');

#profs e turmas
Route::get('/Professor-Turma', 'ProfTurmaController@index')->name('prof-turma');
Route::get('/Professor-Turma/Create', 'ProfTurmaController@create')->name('prof-turma.create');
Route::post('/Professor-Turma/Store', 'ProfTurmaController@store');
Route::get('/Professor-Turma/edit/{id}', 'ProfTurmaController@edit')->name('prof-turma.edit');
Route::post('/Professor-Turma/update/{id}', 'ProfTurmaController@update');
Route::get('/Professor-Turma/delete/{id}', 'ProfTurmaController@destroy');